import { Artigos } from "./artigos.class";
import { Jornauls } from "./journals.class";

export class FastTrack extends Jornauls{
  public publicado: Array<Artigos> = [];
  public nao_publicado: Array<Artigos> = [];
}