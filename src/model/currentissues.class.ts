import { Categoria } from "./categoria.class";
import { Jornauls } from "./journals.class";

export class CurrentIssue extends Jornauls {
  
  public titulo: string;
  public categoria: Categoria;

  /*Atributos opcionais para satisfazer o compilador - dados no atributo categoria*/ 
  categoria_nome?: string;
  categoria_descricao?: string;
  categoria_imagem?: string;

  public ids: number[];
}