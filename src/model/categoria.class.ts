export class Categoria {
  public id?: number; 
  public nome: string;
  public descricao: string;
  public imagem: string
}
