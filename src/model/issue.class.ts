import { Categoria } from "./categoria.class";
import { Jornauls } from "./journals.class";

export class Issue extends Jornauls {
  
  public titulo: string;
  public categoria: Categoria;
  public estado_id: number;
  
}