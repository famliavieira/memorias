import { Artigos } from "./artigos.class";
import { Issue } from "./issue.class";

export class AllIssues {

  public anos: number[] = [];
  public issues: Array<PatternToArrayIssuesAno> = [];

  retornarAnos(){
    return this.anos;
  }

  setIssues(collection:Object){
    for(let ano in collection){
      let ano_number  = Number.parseInt(ano);
      this.anos.push(ano_number);
      this.issues.push({ano:ano_number, issues: collection[ano]});
    }
    this.anos.reverse();
    this.issues.reverse();
  }

  retornarIssuesPorAno(){
    return this.issues;    
  }

}


export class PatternToArrayIssuesAno{
  ano:number;
  issues:Array<Issue> = [];
}