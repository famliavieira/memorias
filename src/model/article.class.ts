export class Article {
  
   public id: number; 
   public artigo_anterior:number; 
   public artigo_proximo:number;  
   public titulo: string;
   public abstract: string; 
   public artigo_nome:string; 
   public conteudo: string;
   public autor_correspondente:string;
   public autores:string;
   public doi: string;
   public endereco_instituicao:string; 
   public pages: string;
   public keywords: [string];
   public downloads: [{nome:string; url:string}];
   public lightbox_imagens: [{id:string, imagem:string}];
   public references: any[];
   public share: {file:string, message:string, subject:string, url:string};


}



