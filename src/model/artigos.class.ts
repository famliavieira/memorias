export class Artigos {
  artigo_nome: string;
  autores: string;
  doi: string;
  id: number;
  pages: string;
  tipo_artigo_id: number;
  tipo_artigo_nome: string;


  total?: number;
}