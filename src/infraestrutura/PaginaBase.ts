import { FormBuilder } from '@angular/forms';
import { NavParams, LoadingController, Loading, ModalController, Modal, AlertController, ToastController, Toast } from 'ionic-angular';
import { ConfiguracaoPaginaBase } from './ConfiguracaoPaginaBase';


export abstract class PaginaBase {
    
    protected _formBuilder: FormBuilder;
    protected _alertCtrl: AlertController;
    protected _loadingCtrl: LoadingController;
    protected _loading: Loading;
    protected _toastCtrl: ToastController;
    protected _toast: Toast;
    protected _modalCtrl: ModalController;
    protected _modal: Modal;
    protected _navParams: NavParams;

    constructor(cpb: ConfiguracaoPaginaBase) {
        this._formBuilder = cpb.formBuilder;
        this._navParams = cpb.navParams;
        this._loadingCtrl = cpb.loadingCtrl;
        this._modalCtrl = cpb.modalCtrl;
        this._alertCtrl = cpb.alertCtrl;
        this._toastCtrl = cpb.toastCtrl;
        
        this.carregarValidadores();
    }

    protected carregarValidadores():void{

        if(this._formBuilder != null){
            this.doCarregarValidadores();
        }

        
    }

    protected mostrarMensagemError(mensagem: string){
        if(this._alertCtrl != null){
            return this._alertCtrl.create({
                title: 'Erro',
                subTitle: mensagem,
                buttons: ['Ok']
            }).present();
        }
    }

    protected mostrarLoading(mensagem: string, duracao: number = 0){
        
        if(duracao==0){

            this._loading = this._loadingCtrl.create({
                content: mensagem
            });

        } else {

            this._loading = this._loadingCtrl.create({
                content: mensagem,
                duration: duracao
            });

        }
        return this._loading.present();
    }


    protected esconderLoading(): void{
        
        if(this._loading!=null){
            this._loading.dismiss();    
        } 
        
    }

     // Notificações em forma de legenda
     protected mostrarToast(mensagem: string){
        
        this._toast = this._toastCtrl.create({
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'Fechar'
        });
        this._toast.setMessage(mensagem);
        this._toast.present();
        
    }

    protected esconderToast(): void{
        
        if(this._toast!=null){
            this._toast.dismiss();
        }
        
    }

    protected mostrarModal(page:any,params: {} = {}){
        
        if(this._modalCtrl!==null ){
            
            this._modal = this._modalCtrl.create(page,params);
            
            return this._modal.present();
        }
    }

    protected fecharModal(){
        return this._modal.dismiss();
    }

    abstract doCarregarValidadores(): void;

    
}