import { FormBuilder } from '@angular/forms';
import { NavParams, LoadingController, ModalController, AlertController, ToastController } from 'ionic-angular';

export interface ConfiguracaoPaginaBase { 
    
    formBuilder?: FormBuilder;
    alertCtrl?: AlertController;
    loadingCtrl?: LoadingController;
    toastCtrl?: ToastController;
    modalCtrl?: ModalController;
    navParams?: NavParams;
    
}