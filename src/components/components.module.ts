import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular'
import { IconFavoriteToogleComponent } from './icon-favorite-toogle/icon-favorite-toogle';
import { SearchListComponent } from './search-list/search-list';
import { SlideAheadComponent } from './slide-ahead/slide-ahead';
import { SlideCurrentIssueComponent } from './slide-current-issue/slide-current-issue';
import { SlideMostReadComponent } from './slide-most-read/slide-most-read';
import { SlideFastTrackComponent } from './slide-fast-track/slide-fast-track';
import { SlideAllIssueComponent } from './slide-all-issue/slide-all-issue';
import { MatExpansionModule } from '@angular/material';
import { SlideRecentPostsComponent } from './slide-recent-posts/slide-recent-posts';
import { PopoverComponent } from './popover/popover';
@NgModule({
    declarations: [
        IconFavoriteToogleComponent,
        SearchListComponent,
        SlideAheadComponent,
        SlideCurrentIssueComponent,
        SlideMostReadComponent,
        SlideFastTrackComponent,
        SlideAllIssueComponent,
    SlideRecentPostsComponent,
    PopoverComponent,
    ],
    imports: [MatExpansionModule,IonicModule],
    exports: [
        IconFavoriteToogleComponent,
        SearchListComponent,
        SlideAheadComponent,
        SlideCurrentIssueComponent,
        SlideMostReadComponent,
        SlideFastTrackComponent,
        SlideAllIssueComponent,
    SlideRecentPostsComponent,
    PopoverComponent,
    ]
})
export class ComponentsModule { }
