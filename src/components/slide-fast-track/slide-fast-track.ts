import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FastTrack } from '../../model/fasttrack.class';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { CurrentIssue } from '../../model/currentissues.class';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the SlideFastTrackComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'slide-fast-track',
  templateUrl: 'slide-fast-track.html'
})
export class SlideFastTrackComponent {

  @Input('expandedAll') expandedAll: boolean = false;

   /* Atributos para manipular Expansion Panel */
   step: number = 0;

  @Output('pegarTotal') EvtPegarTotal: EventEmitter<{}> = new EventEmitter();

  /* Artigos FastTrack of Print */
  public dadosFastTrack: FastTrack;

  constructor(
    private webservice: WebserviceProvider,
    public sharedProvider: SharedProvider,
  ) {
    console.log('Hello SlideFastTrackComponent Component');
    this.dadosFastTrack = new FastTrack();
  }

  ngOnInit() {

      // Consumir endpoint "MostRead" do webservice
      this.webservice
      .listarFastTrack()
      .then((fastTrackObj: FastTrack) => {
        console.log(fastTrackObj)

        this.dadosFastTrack = fastTrackObj;
        this.EvtPegarTotal.emit({ 'name': 'fast-track', 'total': this.dadosFastTrack.total });
      });

  }

}
