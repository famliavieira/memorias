import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { AllIssues, PatternToArrayIssuesAno } from '../../model/allissues.class';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { SharedProvider } from '../../providers/shared/shared';
import { MatAccordion } from '@angular/material';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';

import { PhotoViewer } from '@ionic-native/photo-viewer';

/**
 * Generated class for the SlideAllIssueComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'slide-all-issue',
  templateUrl: 'slide-all-issue.html'
})
export class SlideAllIssueComponent {

  @Input('expandedAll') expandedAll: boolean = false;
  @Input('arrayFavoritos') arrayFavoritos: number[] = [];

  // @ViewChild('accordion') accordion: MatAccordion;

  /* Artigos AllIssues  */
  public dadosAllIssues: AllIssues;

  /* Atributos para manipular Expansion Panel */
  step: number = 0;
 

  arrAllIssues: PatternToArrayIssuesAno[] = [];
  paginacaoIndex: number = 1;

  //declarando variável do expansion
  expansion = 0;

  slideAnoAtualSelected = 0;

  constructor(
    public webservice: WebserviceProvider,
    public sharedProvider: SharedProvider,
    public favorite: FavoriteStorageProvider,
    
  ) {
    console.log('Hello SlideAllIssueComponent Component');
  }

  ngOnInit() {

    // Consumir endpoint "MostRead" do webservice
    this.webservice
      .listarAllIssues()
      .then((allIssuesObj: AllIssues) => {
        this.dadosAllIssues = allIssuesObj;

        this.arrAllIssues = this.dadosAllIssues.retornarIssuesPorAno();


        // for(let i = 0; i<this.arrAllIssues.length; i++){
        //   this.arrAllIssues
        // }

        console.log(`favoritos`,this.favorite.listaIdsFavoritos);


        // pegar current issues do ano atual 
        if(this.arrAllIssues[0]){
          
          let index = this.arrAllIssues[0].issues.findIndex(function(val){
          
            if(val.estado_id==2){
              return true;
            }
          });

          console.log('INDEX',index);

          this.slideAnoAtualSelected = index;
        }

        // console.log('ANOS COM ISSUES',this.dadosAllIssues.retornarIssuesPorAno());

        


      });





  }

  
  public contarFavoritosPorCategoria(ids): number {

    let count = 0;
    let favoritos = this.favorite.listaIdsFavoritos;

    for (let index = 0; index < favoritos.length; index++) {

      const element = favoritos[index];

      if (ids.indexOf(element) != -1) {
        count++;
      }

    }
    return count;

  }


  //Função de deixar o primeiro aberto no All Issue
  setExpansion(index: number) {
    this.expansion = index;

  }

  public doInfinite(evt) {
    
    


    this.paginacaoIndex++;

    // console.log(evt,this.paginacaoIndex);

    this.webservice.listarAllIssues(this.paginacaoIndex).then((objAllIssues: AllIssues) => {

      let arrnovo = objAllIssues.retornarIssuesPorAno();

      this.arrAllIssues = this.arrAllIssues.concat(arrnovo);



      evt.complete();


      if (document.getElementsByClassName('scroll-content')[0]) {
        let teste: any = document.getElementsByClassName('scroll-content')[0];
        // console.log(teste)
        teste.style.marginBottom = '0px';
      }


      // this.detect.markForCheck();
      // this.detect.detectChanges();


    });





  }

  public checarSlides(evt){
    alert(1);
    console.log('checarSlides',evt)
  }
}
