import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Events, LoadingController, AlertController } from 'ionic-angular';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { SharedProvider } from '../../providers/shared/shared';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { CurrentIssue } from '../../model/currentissues.class';
import { Categoria } from '../../model/categoria.class';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';

/**
 * Generated class for the IconFavoriteToogleComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'slide-current-issue',
  templateUrl: 'slide-current-issue.html',
  // host: {
  //   '(click)': 'eventCustom($event)'
  // }
})
export class SlideCurrentIssueComponent {

  @Input('expandedAll') expandedAll: boolean = false;
  @Output('pegarTotal') EvtPegarTotal: EventEmitter<{}> = new EventEmitter();

  /* Artigos Current Issues e sua categoria*/
  public dadosCurrentIssue: CurrentIssue;
  public categoria: Categoria;

  /* Atributos para manipular Expansion Panel */
  public step: number = 0;

  public total_favoritos: number = 0;

  constructor(
    public events: Events,
    public sharedProvider: SharedProvider,
    public webservice: WebserviceProvider,
    public favoriteStorage: FavoriteStorageProvider
  ) {

    this.categoria = new Categoria();
    this.dadosCurrentIssue = new CurrentIssue();

    // this.events.subscribe('slide-ahead:created', (data) => {
    // })

    this.events.subscribe('favoriteStorage:deleted', (obj: any) => {
      this.total_favoritos = this.contarFavoritosPorArtigos();
    })

    this.events.subscribe('favoriteStorage:inserted', (obj: any) => {
      this.total_favoritos = this.contarFavoritosPorArtigos();
    })



  }

  ngOnInit() {

    // Consumir endpoint "Current issue do webservice
    this.webservice
      .listarRevistas()
      .then((objCurrentIssue: CurrentIssue) => {

        // console.log(objCurrentIssue)

        this.dadosCurrentIssue = objCurrentIssue;

        this.EvtPegarTotal.emit({ 'name': 'current-issue', 'total': this.dadosCurrentIssue.total });

        // Popular total de artigos se for o slide 'ahead'
        // if (this.slideAtivoChave == 1) {
        //   this.total_artigos = this.dadosCurrentIssue.total;
        // }

        // this.splashScreen.hide();

        this.favoriteStorage.getAll().then( ()=>{
          this.total_favoritos = this.contarFavoritosPorArtigos();
        })


        this.events.publish('currentIssue:loaded',this.dadosCurrentIssue)
        
    
        

      });

  }

  public contarFavoritosPorArtigos(): number {

    let count = 0;
    let favoritos = this.favoriteStorage.listaIdsFavoritos;

    for (let index = 0; index < favoritos.length; index++) {

      const element = favoritos[index];

      if (this.dadosCurrentIssue.ids.indexOf(element) != -1) {
        count++;
      }

    }
    console.log(count);
    return count;

  }



  /**
   * Seta posicao atual do collapse
   * @param pos 
   */
  public setStep(pos) {
    this.step = pos;
  }

  doCarregarValidadores(): void { }
}
