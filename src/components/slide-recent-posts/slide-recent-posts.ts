import { Component, Input, EventEmitter, Output, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { SharedProvider } from '../../providers/shared/shared';
import { RecentPost } from '../../model/recentpost.class';

/**
 * Generated class for the SlideRecentPostsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'slide-recent-posts',
  templateUrl: 'slide-recent-posts.html'
})
export class SlideRecentPostsComponent {

  public dadosRecentPost: RecentPost;

  @ViewChild('btnViewAll') btnViewAll: ElementRef;

  @Output('pegarTotal') EvtPegarTotal: EventEmitter<{}> = new EventEmitter();
  

  
  @Input('showAllPosts') 
  public allPosts: boolean = false;

  @Output('btnViewAllClicked')
  public btnViewAllClicked: EventEmitter<number> = new EventEmitter();


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private webservice: WebserviceProvider,
    public sharedProvider: SharedProvider,
    public events: Events
  ) {
    this.dadosRecentPost = new RecentPost();

    this.events.subscribe('revista-details:viewAllPostsClicked', (btViewAllclicked)=>{

      console.log(btViewAllclicked);

      if(btViewAllclicked){
        this.EvtPegarTotal.emit({ 'name': 'recent-posts', 'total': this.dadosRecentPost.total });
      }
    })
    
  }

  ionViewDidLoad() {
    
  }




  ngOnInit(){
    // Consumir endpoint "RecentPost" do webservice
    this.webservice
      .listarRecentPost()
      .then((resJsonRecentPost: RecentPost) => {

        console.log(resJsonRecentPost);
        this.dadosRecentPost = resJsonRecentPost;


      });

      this.EvtPegarTotal.emit({ 'name': 'recent-posts', 'total': 10 });
  }

  public viewAll(){

    this.EvtPegarTotal.emit({ 'name': 'recent-posts', 'total': this.dadosRecentPost.total });
    this.allPosts = true;
    this.btnViewAll.nativeElement.style.display = 'none';    // document.getElementById('bt_viewAllRecentPosts').style.display = 'none';

    
  }
  
}
