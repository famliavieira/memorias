import { Component, Input } from '@angular/core';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the SearchListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'search-list',
  templateUrl: 'search-list.html'
})
export class SearchListComponent {

  text: string;
  
  @Input('dados') dados: Array<any> = [];

  constructor(
    public sharedProvider: SharedProvider
  ) {
    console.log('Hello SearchListComponent Component');
    this.text = 'Hello World';
  }

}
