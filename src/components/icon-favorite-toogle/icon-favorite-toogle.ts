import { Component, Input } from '@angular/core';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';
import { Events, LoadingController, AlertController } from 'ionic-angular';
import { PaginaBase } from '../../infraestrutura/PaginaBase';

/**
 * Generated class for the IconFavoriteToogleComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'icon-favorite-toogle',
  templateUrl: 'icon-favorite-toogle.html',
  host: {
    '(click)': 'favoritarToogle($event)'
  }
})
export class IconFavoriteToogleComponent extends PaginaBase {

  @Input('name') iconName: string;
  @Input(`articleId`) articleId: number;

  // iconName: string = "";
  isFavorited: boolean;

  nameAux: string;

  constructor(
    public favoriteStorage: FavoriteStorageProvider,
    public events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {
    super({ loadingCtrl: loadingCtrl, alertCtrl: alertCtrl });
    this.bootstrapComponent();
   
  }

  ngOnInit() {
    // console.log(`component article ID  `, this.articleId);
    this.nameAux = this.iconName;
    // console.log(`name aux`,this.nameAux);
    this.toogle();
  }

  public favoritarToogle(evt: MouseEvent) {

    // console.log(`ESTA FAVORITADO ?`,this.isFavorited)

    // se ja favoritado, sera removido da lista.
    if (this.isFavorited) {
      let alert = this._alertCtrl.create({
        title: 'Reading List',
        message: 'Confirm removal from your reading list?',
        buttons: [
          {
            text: 'Remove?',
            handler: () => {
              // confirma remocao              
              this.favoriteStorage.salvar(this.articleId).then(() => {
                this.toogle();
              });
              return true;
            }
          },
          {
            text: 'cancelar',
            role: 'cancel',
            handler: () => {
              // nada a fazer
            }
          }
        ]
      });
      alert.present();
    } else {

      // caso nao esteja favoritado, favoritar!
      this.favoriteStorage.salvar(this.articleId).then(() => {
        this.toogle();
      });

    }

    evt.stopPropagation();
  }

  toogle() {

    // verificar no local Storage se esta favoritado
    if (!this.favoriteStorage.isFavorite(this.articleId)) {

      this.isFavorited = false;

      // input name vazio
      this.iconName = "custom-favorite";

    } else {

      this.isFavorited = true;

      // input name vazio
      if (!this.nameAux) {
        this.iconName = "custom-favoritefull"
      } else {
        console.log(this.nameAux);
        this.iconName = this.nameAux;
      }

    }

  }

  public removerArticle(evt: MouseEvent, artigo_id: number) {
    evt.stopPropagation();
  }


  private bootstrapComponent(){
     // console.log('Hello IconFavoriteToogleComponent Component');
     this.events.subscribe('favoriteStorage:deleted', (data) => {

      if (data.artigo_id === this.articleId) {
        console.log(data);
        this.toogle();
      }
    });

    // console.log('Hello IconFavoriteToogleComponent Component');
    this.events.subscribe('favoriteStorage:inserted', (data) => {

      if (data.artigo_id === this.articleId) {
        console.log(data);
        this.toogle();
      }



    });
  }

  doCarregarValidadores(): void { }
}
