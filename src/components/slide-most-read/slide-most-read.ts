import { Component, EventEmitter, Output, Input } from '@angular/core';
import { MostRead } from '../../model/mostread.class';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the SlideMostReadComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'slide-most-read',
  templateUrl: 'slide-most-read.html'
})
export class SlideMostReadComponent {

  @Input('expandedAll') expandedAll: boolean = false;
  @Output('pegarTotal') EvtPegarTotal: EventEmitter<{}> = new EventEmitter();

  /* Artigos MostRead of Print */
  public dadosMostRead: MostRead;

  viewAllMostReadBoolean = false;

  total_artigos: number = 0;

  constructor(
    public webservice: WebserviceProvider,
    public sharedProvider: SharedProvider
  ) {
    console.log('Hello SlideMostReadComponent Component');
    this.dadosMostRead = new MostRead();
  }

  ngOnInit() {
    // Consumir endpoint "MostRead" do webservice
    this.webservice
      .listarMostRead().then( (objMostRead:MostRead)=>{
        this.dadosMostRead = objMostRead
      }) 
      
  }



  
}
