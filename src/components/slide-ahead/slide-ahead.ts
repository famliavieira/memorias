import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';
import { Events, LoadingController, AlertController } from 'ionic-angular';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { AheadOfPrint } from '../../model/aheadofprint.class';
import { SharedProvider } from '../../providers/shared/shared';
import { WebserviceProvider } from '../../providers/webservice/webservice';

/**
 * Generated class for the IconFavoriteToogleComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'slide-ahead',
  templateUrl: 'slide-ahead.html',
  // host: {
  //   '(click)': 'eventCustom($event)'
  // }
})
export class SlideAheadComponent  {
  
  @Input('expandedAll') expandedAll: boolean = false;
  @Output('pegarTotal') EvtPegarTotal:EventEmitter<{}> = new EventEmitter();

  /* Artigos Ahead of Print */
  public dadosAheadOfPrint: AheadOfPrint;

  constructor(
    // public events: Events,
    public sharedProvider: SharedProvider,
    public webservice: WebserviceProvider
  ) {
    this.dadosAheadOfPrint = new AheadOfPrint();
    // this.events.subscribe('slide-ahead:created', (data) => {
    // })
  }

  ngOnInit() {

    // Consumir endpoint "Ahead Print" do webservice
    this.webservice
      .listarAheadOfPrint()
      .then((resJsonAhead: AheadOfPrint) => {

        this.dadosAheadOfPrint = resJsonAhead;

        console.log('AHEAD',this.dadosAheadOfPrint);
        // Emite evento para o component em (pegarTotal), quando webservice entregar Promise
        this.EvtPegarTotal.emit({name:'ahead',total:resJsonAhead.total});

      });

    
  }

}
