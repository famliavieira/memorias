// NATIVE PLUGINS E MODULOS IONIC
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';

// PROVIDERS
import { WebserviceProvider } from '../providers/webservice/webservice';
import { HttpClientModule } from '@angular/common/http';
import { SharedProvider } from '../providers/shared/shared';

// MODULES

// INCLUSAO DO Angular Material MODULES
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatExpansionModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';

// PAGES
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { RevistasPage } from '../pages/revistas/revistas';
import { SearchPage } from '../pages/search/search';
import { ExplorePage } from '../pages/explore/explore';
import { FavoritePage } from '../pages/favorite/favorite';
import { RevistaDetailsPage } from '../pages/revista-details/revista-details';
import { FavoriteStorageProvider } from '../providers/favorite-storage/favorite-storage';
import { CategoryPage } from '../pages/category/category';
import { ComponentsModule } from '../components/components.module';
import { ArticlePage } from '../pages/article/article';
import { EditorialPolicyPage } from '../pages/editorial-policy/editorial-policy';
import { MemoriasBoardPage } from '../pages/memorias-board/memorias-board';
import { RecentPostsPage } from '../pages/recent-posts/recent-posts';

// Plugin social sharing
import { SocialSharing } from '@ionic-native/social-sharing';


import { LocationStrategy,
  PathLocationStrategy } from '@angular/common';
import { PhotoViewer } from '@ionic-native/photo-viewer';


// PIPES
import { SafeHtmlPipe } from '../pipes/safehtml.pipe';
import { PostPage } from '../pages/post/post';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    RevistasPage,
    SearchPage,
    ExplorePage,
    FavoritePage,
    RevistaDetailsPage,
    ArticlePage,
    CategoryPage,
    RecentPostsPage,
    EditorialPolicyPage,
    MemoriasBoardPage,
    PostPage,
    SafeHtmlPipe
  ],
  imports: [
    BrowserAnimationsModule,
    ComponentsModule,
    MatExpansionModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressBarModule,
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    RevistasPage,
    SearchPage,
    ExplorePage,
    FavoritePage,
    RevistaDetailsPage,
    ArticlePage,
    CategoryPage,
    RecentPostsPage,
    MemoriasBoardPage,
    EditorialPolicyPage,
    PostPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    WebserviceProvider,
    SharedProvider,
    FavoriteStorageProvider,
    PhotoViewer,
    SocialSharing,
    { provide: LocationStrategy, useClass: PathLocationStrategy},
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})
export class AppModule { }
