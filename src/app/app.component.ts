import { Component } from '@angular/core';
import { Platform, LoadingController, Events, NavController, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { FavoriteStorageProvider } from '../providers/favorite-storage/favorite-storage';
import { PaginaBase } from '../infraestrutura/PaginaBase';
import { RevistaDetailsPage } from '../pages/revista-details/revista-details';

@Component({
  templateUrl: 'app.html'
})
export class MyApp extends PaginaBase {
  rootPage: any = TabsPage;

  constructor(
    public app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private event: Events,
    private favoriteStorage: FavoriteStorageProvider,
    private alertCtrl: AlertController,
    loadingCtrl:LoadingController
  ) {
    super({ loadingCtrl: loadingCtrl, alertCtrl: alertCtrl });
    this.initializeApp();
  }
  
  doCarregarValidadores():void{}

  private initializeApp() {

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
    });

    // Configurar comportamento do hardware backbuttonAction 
    this.initializeRegisterBackButtonAction();

    // forçar liberar tela splashScreen quando o webservice currentIssue estiver carregado
    this.event.subscribe('currentIssue:loaded', (dadosCurrentIssue) => {
      console.log(`CurrentIssue LOADED`, dadosCurrentIssue);
      this.splashScreen.hide();
    });

    // ID FAVORITADOS
    this.favoriteStorage.listaIdsFavoritos;


  }

  


  /**
   * Configura o comportamento do botao backbutton.
   * Se estiver na pagina Jornauls, ele ira perguntar se realmente quer sair.
   */
  private initializeRegisterBackButtonAction(){
    
    // Manipular acao padrao no backButton do Android/windows
    this.platform.registerBackButtonAction(() => {

      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();

      // console.log(nav);
      // console.log(activeView.name);
      // console.log('canGoBack', nav.canGoBack());

      if (activeView.name == 'ModalCmp') { // Fechar se for Modal
        activeView.dismiss();
      } else if (activeView.name == 'RevistaDetailsPage') { // Perguntar se for HomePage(Journals)

        if (nav.canGoBack()) {
          nav.pop();
        } else {

          if(activeView.instance.slideAtivoChave != 1){
            activeView.instance.goToSlide(1);
          } else {

            const alert = this.alertCtrl.create({
              title: 'Close APP',
              message: 'Are you sure?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  //  this.nav.setRoot('HomePage');
                  console.log('** Saída do App Cancelada! **');
                }
              }, {
                text: 'Close APP',
                handler: () => {
                  //  this.logout();
                  this.platform.exitApp();
                }
              }]
            });
            alert.present();

          }
          

         

        }

      } else {
        // console.log('nao modal OU revistaDetails', nav.parent);
        nav.parent.viewCtrl.instance.toFirstTab();
      }

      this.esconderLoading();

    });
  }

  public topo: string = "<ion-header><ion-navbar><ion-title>teste</ion-title></ion-navbar></ion-header>";
}
