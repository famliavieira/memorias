import { Component, ViewChild, Input, ElementRef, Output, EventEmitter, ViewChildren, QueryList, AfterViewInit, ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, Range, AlertController, Content, Alert } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { Article } from '../../model/article.class';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the ArticlePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-article',
  templateUrl: 'article.html',
})
export class ArticlePage extends PaginaBase implements AfterViewInit {

  article_pai: number;
  article: Article;
  isLoading: boolean = false;

  text_ref:any = '';
  links_ref:any = '';

  array_references: Array<any> = [];
  
  @Output() public readonly contentChanged = new EventEmitter<string>();
  @ViewChild('fontsizerange') fontsizerange: Range;
  @ViewChild("conteudo") contentWrapper: ElementRef;
  // Content View
  @ViewChild(Content) contentView: Content;
  content = "";


  bindAllEventsLightbox:boolean = false;


  reference_nome: string = "TEXTO DO POPOVER"


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public webservice: WebserviceProvider,
    public loadingCtrl: LoadingController,
    public favoriteProvider: FavoriteStorageProvider,
    public elRef: ElementRef,
    public detectChanges: ChangeDetectorRef,
    public photoViewer: PhotoViewer,
    public socialSharing: SocialSharing,
    public alertCtrl: AlertController
  ) {
    super({
      loadingCtrl: loadingCtrl,
    });
    // if(this.fontsizerange.ionChange){
    //   console.log('valor atual',this.fontsizerange.pin)
    // }
    console.log(this.photoViewer);
    // .addEventListener('click', (event)=>{
    //   console.log(event);
    // });

    

    


  }

  ngAfterViewInit(): void {


  }


  // monitora moficacoes ate depois da tela carregada(ajax)
  ngAfterContentChecked(): void {


    // if(document.querySelectorAll('a[href^="#references_"]').length>0){
    //   this.injetarReferencias();
    // }

    // anexando clicks em links com nome iniciando com lightbox(TABLE X) 
    if(document.querySelectorAll('a[id^="lightbox_"]').length>0){
      this.anexarEventoClickParaLightbox();
    }
    

      // <a href="fig01">
/*       let pattern_fig = /^#f[0-9]+/;
      if (pattern_fig.test(anchor.getAttribute('href'))) {
        this.anexarEventoClickParaImagens(anchor);
      } */


      this.verify();

      



  }

  anexarEventoClickParaLightbox(){

    if(!this.bindAllEventsLightbox){
      
      this.bindAllEventsLightbox = true;

      var $this = this;

      let anchors_imagem = document.querySelectorAll('a[id^="lightbox_"]');
  
      for (let i = 0; i < anchors_imagem.length; i++) {
  
        let anchor: any = anchors_imagem[i];

        
        
  
        let pattern_lightbox = /^lightbox_/;
        
        if (pattern_lightbox.test(anchor.getAttribute('id'))) {

          let ID = anchor.getAttribute('id');
          let DESCRICAO = anchor.innerText;
          let arr_imagens = $this.article.lightbox_imagens;
          
          console.log(ID)

          // click
          anchor.onclick = function (event) {
            event.preventDefault();
    
            let indexImagem = arr_imagens.findIndex(function(value){
              return value.id == ID
            });

            let url_imagem = arr_imagens[indexImagem].imagem;

            console.log(ID,arr_imagens,url_imagem);

            if(url_imagem){
              console.log(anchor);
              $this.photoViewer.show(url_imagem, DESCRICAO);

            }
            event.stopImmediatePropagation();
          }
    
          
        }
  
  
  
  
      }
      
    }

   







    
  }

  anexarEventoClickParaImagens(anchor:any){

    var $this = this;
    // pattern id
  //  let pattern = /^#f[0-9]+/;
//    if (pattern.test(anchor.getAttribute('href'))) {

      anchor.onclick = function (event) {
        event.preventDefault();

        let ID = "";          
        if(anchor.getAttribute('id')){
          ID = anchor.anchor.getAttribute('id');
        }


        console.log(ID);

        let imagem = $this.article.lightbox_imagens[0].imagem || null;  
        if(imagem){
          console.log(anchor);
          $this.photoViewer.show(imagem, 'ok');
        }
        
        event.stopImmediatePropagation();
      }

    //}
    
  }


  ionViewDidLoad() {
    // console.log('ionViewDidLoad ArticlePage');
    if (this.navParams.get('artigo_id')) {
      this.article_pai = this.navParams.get('artigo_id');
      console.log('artigo pai', this.article_pai);
      this.pegarDadosArticle(this.article_pai);
    }

    // this.correspondente();
    this.share();
    this.openFontsize();

    // chamar centralização do popover
    // this.centerpop();
    
    

  }

  favoritar() {
    console.log(this.article.id);
    // return;
    // this.favoriteProvider.salvar(this.article.id);
  }

  //Pegando o article.title separadamente
  getArticleTitle(x){
    let artigo:any = this.article.titulo;
    let index = artigo.indexOf('|');
    if(x == "antes"){
      let texto = artigo.slice(0,index);
      return texto;
    }else if(x == "depois"){
      let texto = artigo.slice(index+1);
      return texto;
    }
  }
  

  pegarDadosArticle(idArticle: number, direcao: string = "") {

    console.log('ID', idArticle, 'FOI PRA ', direcao);

    // nao permitir consumir api se nao tiver mais paginas a direita e esquerda
    if ((direcao == 'esquerda' && this.article.artigo_anterior == null) || (direcao == 'direita' && this.article.artigo_proximo == null)) {
      return null;
    }

    this.isLoading = false;

    this.mostrarLoading('Loading').then(() => {

      this.webservice.getArticle(idArticle).then((data: any) => {
        console.log('ARTICLE', data);

        this.article = data;
        this.isLoading = true;


        this.array_references = this.article.references;

        this.esconderLoading();
        
      })
    })


  }

  // pre-suponho que seja assim
  openTab(url) {
    if (url) {
      window.open(url, '_system');
    }

  }

  // abstract method implemented by PaginaBase
  doCarregarValidadores(): void { }

  //controlador do fontsize, função chamada sempre que muda o fontsize
  fontsize: number = 0;
  controlSize() {
    let textop: any = document.getElementsByClassName('textopequeno');
    let textom: any = document.getElementsByClassName('textomedio');
    let textog: any = document.getElementsByClassName('textogrande');

    switch (this.fontsize) {
      case 0:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "0.9em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.1em";
        }
        break;
      case 1:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.1em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.2em";
        }
        break;
      case 2:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1.1em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.2em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.3em";
        }
        break;
      case 3:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1.2em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.3em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.4em";
        }
        break;
      case 4:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1.3em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.4em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.5em";
        }
        break;
    }
  }

  // Chama o font-size pra cima com animação
  public openfontsizeClicked: boolean = false;
  //chama Share
  public openShareClicked: boolean = false;
  // Declara variável correspondente
  public openCorrespClicked: boolean = false;
  verificacao(x) {

    if (x == 'fontsize') {
      this.openfontsizeClicked = !this.openfontsizeClicked;
      this.openShareClicked = false;
      this.openCorrespClicked = false;
    } else if (x == 'share') {
      this.openShareClicked = !this.openShareClicked;
      this.openfontsizeClicked = false;
      this.openCorrespClicked = false;
    } else if (x == 'correspondente'){
      this.openCorrespClicked = !this.openCorrespClicked;
      this.openfontsizeClicked = false;
      this.openShareClicked = false;
    }
    this.openFontsize();
    this.share();
    this.correspondente();
  }

  openFontsize() {
    let div: any = document.getElementById('fontsize');
    let toolbar: any = document.getElementById('toolbar');

    if (this.openfontsizeClicked == true) {
      div.style.bottom = toolbar.offsetHeight + 'px';
      div.style.opacity = '1';
    } else {
      div.style.bottom = '-' + div.offsetHeight + 'px';
      div.style.opacity = '0';
    }
  }

  share(redesocial?) {
    // let div: any = document.getElementById('share');
    // let toolbar: any = document.getElementById('toolbar');

    // if (this.openShareClicked == true) {
    //   div.style.bottom = toolbar.offsetHeight + 'px';
    //   div.style.opacity = '1';
    // } else {
    //   div.style.bottom = '-' + div.offsetHeight + 'px';
    //   div.style.opacity = '0';
    // }
  }

  correspondente() {
    let div: any = document.getElementById('correspondente');
    let toolbar: any = document.getElementById('toolbar');

    if (this.openCorrespClicked == true) {
      div.style.bottom = toolbar.offsetHeight + 'px';
      div.style.opacity = '1';
    } else {
      div.style.bottom = '-' + div.offsetHeight + 'px';
      div.style.opacity = '0';
    }
  }

  injetarReferencias() {

    if (this.contentWrapper) {

      let $contextParent = this;

      if (this.content != this.contentWrapper.nativeElement.innerHTML) {

        this.content = this.contentWrapper.nativeElement.innerHTML
        const c = this.contentWrapper.nativeElement.innerHTML;

        // RegEx OU Expression Regular
        // let pattern: RegExp = /\(([0-9]+)\)/g;
        // let pattern: RegExp = /^([0-9]+)/g;
        let pattern: RegExp = /^(#references_)/g;

        var anchors:any = document.querySelectorAll('a[href^="#references_"]');

        for (let i = 0; i < anchors.length; i++) {

          if(!anchors[i]){
            return false;
          }

          var href:string = anchors[i].getAttribute('href');
          var id:string = anchors[i].getAttribute('id');
          console.log(id);
          
          document.getElementById(id).onclick = function(e){
            
            e.preventDefault();

             
            if(!e.target){
              return false;
            }
            
            let target:any = e.target;

            let obj_reference = $contextParent.array_references.find(function(reference){
              return reference.ordem == target.getAttribute('id');
            });

            console.log(obj_reference);

            $contextParent.reference_nome = obj_reference.nome; 
            // $contextParent.popover('open');

            console.log(e)
            
            e.stopImmediatePropagation();
            return;
          }


          // console.log(sup.firstElementChild.setAttribute('href',''));

          // pegar apenas id
          // if (pattern.test(href)) {
            // console.log(anchors[i])

            

            // sup.innerHTML = "<a href='#references_"+id+"'>"+id+"</a>";
            // tag_a.setAttribute('href','#');



            // anchors[i].onclick = function(e){

              

              // e.preventDefault();
              // if(e.target){
              //   tag_a = e.target;
              //   console.log(tag_a);
              // }

              // var id = stringTest.replace(/(\(|\))/gi, "");

              // var referenciaDivAnchor = 'references_'+id;
              // $contextParent.scrollTo(referenciaDivAnchor);


              // var id = tag_a.getAttribute('id');
              // console.log('entrei',id);
              
              // let index = this.array_references.findIndex(function(value){
              //   return value.ordem == id;
              // })
              // console.log(this.array_references[index]);
              

            //   e.stopImmediatePropagation();
            //   return false;
            // }

          // }
        }
      }
    }

  }

  scrollTo(elementId: string) {
    this.detectChanges.detectChanges();
    window.location.hash = elementId;
  }

  // SOCIAL SHARING
  sharing(redesocial){


    console.log(this.article);
    // SOCIAL SHARING
    let url = this.article.share.url;
    let message = this.article.share.message;
    let file = this.article.share.file;
    let subject = this.article.share.subject;
    // let teste = this.article.autor_correspondente.replace(/<[^>]*>/g, '');
    switch(redesocial){
      case 'facebook':
        this.socialSharing.shareViaFacebook(message, null, url);
      break;
      case 'whatsapp':
          this.socialSharing.shareViaWhatsApp(message, file, url);
      break;
      case 'twitter':
          this.socialSharing.shareViaTwitter(message, file, url);
      break;
      case 'outros':
          this.socialSharing.share(message, subject, null, url);
      break;
    }

  }

  // Chamar Alert da referências
  refalert(id){
    const alert = this.alertCtrl.create({
      title: 'Referência 1',
      // subTitle: this.article.references[0].nome,
      buttons: ['Fechar']
    });
    alert.present();

  }

    //centralização do popover
    centerpop(x?){
      let elementopai:any = document.getElementById('popover').offsetHeight;
      let elemento:any = document.getElementById('popover-center');
      // let heightElment = elemento.offsetHeight;

      let elemento_height:number = (elemento.offsetHeight/2);

    
      setTimeout(()=>{
        elemento.style.top = (elementopai/2) - Math.round((elemento.offsetHeight/2)) + 'px';  
        // console.log(elemento.offsetHeight);
      },100);

      
      let index = this.article.references.findIndex(function(array){
        return array.ordem == x;
      });  
      
      // console.log('index do bagulho aqui '+index)

      this.text_ref = this.article.references[index].nome;

      // LINKS EXTERNOS PARA POR NO MODAL DAS REFERÊNCIAS
      this.links_ref = this.article.references[index].links_externos;
      // this.links_ref = ['www.google.com.br','www.youtube.com.br'];
    }

    //chamar popover
    popover(ordem){
      let elementopai:any = document.getElementById('popover');
        
      // console.log(elementopai);
        elementopai.style.display = "block";
        if(elementopai.style.display == "block"){
          setTimeout(function(){
            elementopai.style.opacity = "1";
          },100);
          this.centerpop(ordem);
          
        }
        
    }
    closepopover(){

      let elementopai:any = document.getElementById('popover');
      elementopai.style.opacity = "0";
        if(elementopai.style.opacity == "0"){
          setTimeout(function(){
            elementopai.style.display = "none";  
          }, 500);
          
        }
    }


    // Função que verifica os parâmetros e aplica a função click
    verify(){ 

      let elementos_pai:any = document.getElementsByTagName('sup');
      let elementos:any = document.getElementsByTagName('a');

      // Loop para pegar todos os elementos com a Tag <a>
      for(let i = 0; i < elementos.length; i++){

        //Pega id do elemento <a>
        let elemento_id:any = elementos[i].getAttribute('data-id');
        //Pega o nome da tag do Pai
        let elemento_name = elementos[i].parentElement.localName;


        // Filtra para saber se ele é filho da tag <sup>
        if(elemento_name == 'sup'){
          //Aplica a função click chamando a função popover com base no ID buscado.
          elementos[i].addEventListener("click", (e)=>{
            this.popover(elemento_id);
            
            // impede de bugar a função popover
            e.stopImmediatePropagation();
          }); 
          // elementos[i].onclick = function(e){
          //   this.popover(elemento_id);
          //   e.stopImmediatePropagation();
          // } 
        }else{
          
        }
      }
    }

}
