import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditorialPolicyPage } from './editorial-policy';

@NgModule({
  declarations: [
    EditorialPolicyPage,
  ],
  imports: [
    IonicPageModule.forChild(EditorialPolicyPage),
  ],
})
export class EditorialPolicyPageModule {}
