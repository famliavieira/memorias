import { Component, ViewChild, Input, ElementRef, Output, EventEmitter, ViewChildren, QueryList, AfterViewInit, ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, Range, AlertController, Content, Alert } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Post } from '../../model/post.class';

/**
 * Generated class for the ArticlePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage extends PaginaBase implements AfterViewInit {

  post_pai: number;
  post: Post;
  isLoading: boolean = false;

  text_ref:any = '';
  links_ref:any = '';

  array_references: Array<any> = [];
  
  @Output() public readonly contentChanged = new EventEmitter<string>();
  @ViewChild('fontsizerange') fontsizerange: Range;
  @ViewChild("conteudo") contentWrapper: ElementRef;
  // Content View
  @ViewChild(Content) contentView: Content;
  content = "";
  bindAllEventsLightbox:boolean = false;
  reference_nome: string = "TEXTO DO POPOVER";
  data_pub: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public webservice: WebserviceProvider,
    public loadingCtrl: LoadingController,
    public favoriteProvider: FavoriteStorageProvider,
    public elRef: ElementRef,
    public detectChanges: ChangeDetectorRef,
    public photoViewer: PhotoViewer,
    public socialSharing: SocialSharing,
    public alertCtrl: AlertController
  ) {
    super({
      loadingCtrl: loadingCtrl,
    });
    // if(this.fontsizerange.ionChange){
    //   console.log('valor atual',this.fontsizerange.pin)
    // }
    console.log(this.photoViewer);
    // .addEventListener('click', (event)=>{
    //   console.log(event);
    // });


  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad PostPage');
    if (this.navParams.get('post_id')) {
      this.post_pai = this.navParams.get('post_id');
      this.pegarDadosPost(this.post_pai);
    }

    // this.correspondente();
    this.share();
    this.openFontsize();

    // chamar centralização do popover
    // this.centerpop();
  }


  ngAfterViewInit(): void {

  }


  // monitora moficacoes ate depois da tela carregada(ajax)
  ngAfterContentChecked(): void {


    // if(document.querySelectorAll('a[href^="#references_"]').length>0){
    //   this.injetarReferencias();
    // }

    // anexando clicks em links com nome iniciando com lightbox(TABLE X) 
    if(document.querySelectorAll('a[id^="lightbox_"]').length>0){
      this.anexarEventoClickParaLightbox();
    }
    

      // <a href="fig01">
/*       let pattern_fig = /^#f[0-9]+/;
      if (pattern_fig.test(anchor.getAttribute('href'))) {
        this.anexarEventoClickParaImagens(anchor);
      } */


      this.verify();

  }

  anexarEventoClickParaLightbox(){

    if(!this.bindAllEventsLightbox){
      
      this.bindAllEventsLightbox = true;

      var $this = this;

      let anchors_imagem = document.querySelectorAll('a[id^="lightbox_"]');
  
      for (let i = 0; i < anchors_imagem.length; i++) {
  
        let anchor: any = anchors_imagem[i];
  
        let pattern_lightbox = /^lightbox_/;
        
        if (pattern_lightbox.test(anchor.getAttribute('id'))) {

          let ID = anchor.getAttribute('id');
          let DESCRICAO = anchor.innerText;
          
          // click
          anchor.onclick = function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
          }
    
          
        }
  
      }
      
    }
    
  }

  anexarEventoClickParaImagens(anchor:any){

    var $this = this;
    // pattern id
  //  let pattern = /^#f[0-9]+/;
//    if (pattern.test(anchor.getAttribute('href'))) {

      anchor.onclick = function (event) {
        event.preventDefault();

        let ID = "";          
        if(anchor.getAttribute('id')){
          ID = anchor.anchor.getAttribute('id');
        }

        event.stopImmediatePropagation();
      }

    //}
    
  }
  
  favoritar() {
    console.log(this.post.id);
    // return;
    // this.favoriteProvider.salvar(this.post.id);
  }

  //Pegando o post.title separadamente
  getPostTitle(x){
    let post:any = this.post.nome;
    let index = post.indexOf('|');
    if(x == "antes"){
      let texto = post.slice(0,index);
      return texto;
    }else if(x == "depois"){
      let texto = post.slice(index+1);
      return texto;
    }
  }
  

  pegarDadosPost(idPost: number, direcao: string = "") {

    this.isLoading = false;

    this.mostrarLoading('Loading').then(() => {

      this.webservice.getPost(idPost).then((data: any) => {

        this.post = data;
        this.isLoading = true;

        this.esconderLoading();
        
      })
    })


  }

  // pre-suponho que seja assim
  openTab(url) {
    if (url) {
      window.open(url, '_system');
    }

  }

  // abstract method implemented by PaginaBase
  doCarregarValidadores(): void { }

  //controlador do fontsize, função chamada sempre que muda o fontsize
  fontsize: number = 0;
  controlSize() {
    let textop: any = document.getElementsByClassName('textopequeno');
    let textom: any = document.getElementsByClassName('textomedio');
    let textog: any = document.getElementsByClassName('textogrande');

    switch (this.fontsize) {
      case 0:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "0.9em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.1em";
        }
        break;
      case 1:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.1em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.2em";
        }
        break;
      case 2:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1.1em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.2em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.3em";
        }
        break;
      case 3:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1.2em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.3em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.4em";
        }
        break;
      case 4:
        for (let i = 0; i < textop.length; i++) {
          textop[i].style.fontSize = "1.3em";
        }
        for (let i = 0; i < textom.length; i++) {
          textom[i].style.fontSize = "1.4em";
        }
        for (let i = 0; i < textog.length; i++) {
          textog[i].style.fontSize = "1.5em";
        }
        break;
    }
  }

  // Chama o font-size pra cima com animação
  public openfontsizeClicked: boolean = false;
  //chama Share
  public openShareClicked: boolean = false;
  // Declara variável correspondente
  public openCorrespClicked: boolean = false;
  verificacao(x) {

    if (x == 'fontsize') {
      this.openfontsizeClicked = !this.openfontsizeClicked;
      this.openShareClicked = false;
      this.openCorrespClicked = false;
    } else if (x == 'share') {
      this.openShareClicked = !this.openShareClicked;
      this.openfontsizeClicked = false;
      this.openCorrespClicked = false;
    } else if (x == 'correspondente'){
      this.openCorrespClicked = !this.openCorrespClicked;
      this.openfontsizeClicked = false;
      this.openShareClicked = false;
    }
    this.openFontsize();
    this.share();
    this.correspondente();
  }

  openFontsize() {
    let div: any = document.getElementById('fontsize');
    let toolbar: any = document.getElementById('toolbar');

    if (this.openfontsizeClicked == true) {
      div.style.bottom = toolbar.offsetHeight + 'px';
      div.style.opacity = '1';
    } else {
      div.style.bottom = '-' + div.offsetHeight + 'px';
      div.style.opacity = '0';
    }
  }

  share(redesocial?) {
    // let div: any = document.getElementById('share');
    // let toolbar: any = document.getElementById('toolbar');

    // if (this.openShareClicked == true) {
    //   div.style.bottom = toolbar.offsetHeight + 'px';
    //   div.style.opacity = '1';
    // } else {
    //   div.style.bottom = '-' + div.offsetHeight + 'px';
    //   div.style.opacity = '0';
    // }
  }

  correspondente() {
    let div: any = document.getElementById('correspondente');
    let toolbar: any = document.getElementById('toolbar');

    if (this.openCorrespClicked == true) {
      div.style.bottom = toolbar.offsetHeight + 'px';
      div.style.opacity = '1';
    } else {
      div.style.bottom = '-' + div.offsetHeight + 'px';
      div.style.opacity = '0';
    }
  }

  injetarReferencias() {

    if (this.contentWrapper) {

      let $contextParent = this;

      if (this.content != this.contentWrapper.nativeElement.innerHTML) {

        this.content = this.contentWrapper.nativeElement.innerHTML
        const c = this.contentWrapper.nativeElement.innerHTML;

        // RegEx OU Expression Regular
        // let pattern: RegExp = /\(([0-9]+)\)/g;
        // let pattern: RegExp = /^([0-9]+)/g;
        let pattern: RegExp = /^(#references_)/g;

        var anchors:any = document.querySelectorAll('a[href^="#references_"]');

        for (let i = 0; i < anchors.length; i++) {

          if(!anchors[i]){
            return false;
          }

          var href:string = anchors[i].getAttribute('href');
          var id:string = anchors[i].getAttribute('id');
          
          document.getElementById(id).onclick = function(e){
            
            e.preventDefault();

            if(!e.target){
              return false;
            }
            
            let target:any = e.target;

            let obj_reference = $contextParent.array_references.find(function(reference){
              return reference.ordem == target.getAttribute('id');
            });

            $contextParent.reference_nome = obj_reference.nome; 
            // $contextParent.popover('open');

            e.stopImmediatePropagation();
            return;
          }

        }
      }
    }

  }

  scrollTo(elementId: string) {
    this.detectChanges.detectChanges();
    window.location.hash = elementId;
  }

  // SOCIAL SHARING
  sharing(redesocial){

    // SOCIAL SHARING
    let url = this.post.nome;
    let message = this.post.conteudo;
    let file = this.post.conteudo;
    let subject = this.post.nome;
    // let teste = this.post.autor_correspondente.replace(/<[^>]*>/g, '');
    switch(redesocial){
      case 'facebook':
        this.socialSharing.shareViaFacebook(message, null, url);
      break;
      case 'whatsapp':
          this.socialSharing.shareViaWhatsApp(message, file, url);
      break;
      case 'twitter':
          this.socialSharing.shareViaTwitter(message, file, url);
      break;
      case 'outros':
          this.socialSharing.share(message, subject, null, url);
      break;
    }

  }

  // Chamar Alert da referências
  refalert(id){
    const alert = this.alertCtrl.create({
      title: 'Referência 1',
      // subTitle: this.post.references[0].nome,
      buttons: ['Fechar']
    });
    alert.present();

  }

  //centralização do popover
  centerpop(x?){
    let elementopai:any = document.getElementById('popover').offsetHeight;
    let elemento:any = document.getElementById('popover-center');
    // let heightElment = elemento.offsetHeight;

    let elemento_height:number = (elemento.offsetHeight/2);

  
    setTimeout(()=>{
      elemento.style.top = (elementopai/2) - Math.round((elemento.offsetHeight/2)) + 'px';  
      // console.log(elemento.offsetHeight);
    },100);
    
  }

  //chamar popover
  popover(ordem){
    let elementopai:any = document.getElementById('popover');
      
    // console.log(elementopai);
      elementopai.style.display = "block";
      if(elementopai.style.display == "block"){
        setTimeout(function(){
          elementopai.style.opacity = "1";
        },100);
        this.centerpop(ordem);
        
      }
      
  }
  
  closepopover(){

    let elementopai:any = document.getElementById('popover');
    elementopai.style.opacity = "0";
      if(elementopai.style.opacity == "0"){
        setTimeout(function(){
          elementopai.style.display = "none";  
        }, 500);
        
      }
  }

  // Função que verifica os parâmetros e aplica a função click
  verify(){ 

    let elementos_pai:any = document.getElementsByTagName('sup');
    let elementos:any = document.getElementsByTagName('a');

    // Loop para pegar todos os elementos com a Tag <a>
    for(let i = 0; i < elementos.length; i++){

      //Pega id do elemento <a>
      let elemento_id:any = elementos[i].getAttribute('data-id');
      //Pega o nome da tag do Pai
      let elemento_name = elementos[i].parentElement.localName;


      // Filtra para saber se ele é filho da tag <sup>
      if(elemento_name == 'sup'){
        //Aplica a função click chamando a função popover com base no ID buscado.
        elementos[i].addEventListener("click", (e)=>{
          this.popover(elemento_id);
          
          // impede de bugar a função popover
          e.stopImmediatePropagation();
        }); 
        // elementos[i].onclick = function(e){
        //   this.popover(elemento_id);
        //   e.stopImmediatePropagation();
        // } 
      }else{
        
      }
    }
  }

}
