import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { MostRead } from '../../model/mostread.class';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';
import { SharedProvider } from '../../providers/shared/shared';
import { PaginaBase } from '../../infraestrutura/PaginaBase';

/**
 * Generated class for the FavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
})
export class FavoritePage extends PaginaBase {

  public arrayIdsFavorites: any[] = [];
  public arrayArticlesFavorites: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private webservice: WebserviceProvider,
    public favoriteStorage: FavoriteStorageProvider,
    public sharedProvider: SharedProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    super({
      modalCtrl: modalCtrl,
      alertCtrl: alertCtrl,
      loadingCtrl: loadingCtrl
    });
    // this.webservice.getArticle()

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritePage');
  }
  ngOnInit() {
    // Consumir endpoint "MostRead" do webservice
  }

  ionViewWillEnter() {

    this.mostrarLoading('Loading Reading List').then(() => {

      // this.listarFavoritos();
      this.favoriteStorage.listarFavoritos().then( arr_articles=>{
        this.arrayArticlesFavorites = arr_articles;
        console.log('tudo carregado',arr_articles);

        this.esconderLoading();
      });

    });

  }


  montarArrayArtigosPorTipo(artigos) {

    let array_tipos_artigos = new Array();
    let tiposArtigos = this.listarTiposDeArtigos(artigos);
    let index = 0;

    for (var tipo_id in tiposArtigos) {

      array_tipos_artigos[index] = {
        tipo_nome: tiposArtigos[tipo_id][0].tipo_artigo_nome,
        tipo: tipo_id,
        artigos: tiposArtigos[tipo_id]
      }

      index++;
    }

    return array_tipos_artigos;

  }

  listarTiposDeArtigos(artigos) {
    // pegar os tipos
    let colecaoTiposArquivos = {}

    for (var i = 0; i < artigos.length; i++) {

      if (!colecaoTiposArquivos[artigos[i].tipo_artigo_id]) {
        colecaoTiposArquivos[artigos[i].tipo_artigo_id] = new Array();
      }
      // colecaoTiposArquivos["nome_do_tipo"] = artigos[i]
      colecaoTiposArquivos[artigos[i].tipo_artigo_id].push(artigos[i]);
    }

    return colecaoTiposArquivos;
  }


  listarFavoritos() {

    let arr_promises = [];

    for (var i = 0; i < this.arrayIdsFavorites.length; i++) {

      this.webservice.getArticle(this.arrayIdsFavorites[i]).then((article) => {
        console.log(article);

        this.arrayArticlesFavorites.push(article)

      })
    }

  }

  public removerArticle(evt: MouseEvent, artigo_id: number) {

    let alert = this._alertCtrl.create({
      title: 'Reading List',
      message: 'Confirm removal from your reading list??',
      buttons: [
        {
          text: 'Remover Favorito',
          handler: () => {
            console.log('Buy clicked');

            let index = this.arrayArticlesFavorites.findIndex((artigoX) => {
              return artigoX.id === artigo_id
            });

            this.arrayArticlesFavorites.splice(index, 1);

            console.log(evt, artigo_id);

            this.favoriteStorage.salvar(artigo_id);
          }
        },
        {
          text: 'cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();




    evt.stopPropagation();
  }

  doCarregarValidadores(): void { }

}
