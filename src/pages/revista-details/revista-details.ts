import { Component, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, Content, Gesture, AlertController, ModalController, LoadingController, Events } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { SharedProvider } from '../../providers/shared/shared';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AllIssues, PatternToArrayIssuesAno } from '../../model/allissues.class';

/**
 * Generated class for the RevistaDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revista-details',
  templateUrl: 'revista-details.html',
})
export class RevistaDetailsPage extends PaginaBase {

  /*Itens da navbar*/
  navbar_list: any[] = [
    "Ahead of Print",
    "Current Issue",
    "Most Read",
    "Recent Posts",
    "All Issue",
    "Fast Track"
  ];

  // total artigos da NavBar(de acordo com o slide)
  public total_artigos: number;

  // Objeto com relacao de totais por nome de slide 
  public totais: { name: string, total: number } = { name: "total_artigos", total: 0 };


  /* Atributos para manipular Expansion Panel */
  step: number = 0;
  expandedAll: boolean = false;

  /* Atributos para manipular Slides */
  @ViewChild(Slides) slides: Slides;
  public slideAtivoChave;

  arrAllIssues: PatternToArrayIssuesAno[] = [];
  paginacaoIndex: number = 1;

  expansion:number = 0;

  arrayFavoritos:number[] = [];

  /*MANIPULACAO DO DOM*/
  @ViewChild('navbarDetails') navbarDetails: ElementRef;
  @ViewChild('collapse') collapse: ElementRef;
  @ViewChild('meuslide') meuslide: Slides;

  constructor(
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public sharedProvider: SharedProvider, // Usada para chamar ArticlePage com ID
    public favoriteStorage: FavoriteStorageProvider,
    public detect: ChangeDetectorRef,
    public loadingCtrl: LoadingController,
    public events: Events
  ) {

    // Injetando dependencias do Contrutor pai
    super({ alertCtrl: alertCtrl, modalCtrl: modalCtrl, loadingCtrl:loadingCtrl  });
  }

  ionSelected() {
    console.log("Home Page has been selected");
    // do your stuff here
    // Se o usuário clicar na tab revista-details ja selecionada.
    this.goToSlide(1);
  }

  /**
  *  Hook Event ngOnInit - Quando a view inicia a primeira vez.
  */
  ngOnInit() {
    this.mostrarLoading('Loading User Data');
    // favoriteStorage
    console.log('ids favoritos',this.favoriteStorage.listaIdsFavoritos);
    this.arrayFavoritos = this.favoriteStorage.listaIdsFavoritos
  }

  /**
  *  Hook Event ionViewDidLoad - Chamado 1x ao finalizar carragento total da view.
  */
  ionViewDidLoad() {

    // console.log('ionViewDidLoad DetailsPage');
    // posiciona no segundo slide 
    this.slides.initialSlide = 1;

    // Slide ativo sera o current Issue.
    this.slideAtivoChave = 1;

    /**
     * Evento disparado se realmente o slide for alterado
     */
    this.slides.ionSlideDidChange.subscribe(evt => {

      let currentIndex = this.slides.getActiveIndex();
      // bug fix navegadores - gerando 1 index apos wipe right
      if (currentIndex == this.navbar_list.length) {
        return false;
      }

      this.slideAtivoChave = currentIndex;

      this.collapse.nativeElement.style.display = 'flex';

      // BLOQUEIA OU DESBLOQUEIA TODOS OS SWIPES MENOS O INDICADO
      if (currentIndex == 5) {
        this.slides.lockSwipeToPrev(false);
        this.slides.lockSwipeToNext(true);
      } else if (currentIndex == 0) {
        this.slides.lockSwipeToNext(false);
      } else {
        this.slides.lockSwipes(false);
      }


      // switch(currentIndex)
      switch (currentIndex) {
        case 0: // Ahead
          // this.total_artigos = this.dadosAheadOfPrint.total;
          this.total_artigos = this.totais['ahead'];
          break;
        case 1: // Current issue
          // this.total_artigos = this.dadosCurrentIssue.total;
          this.total_artigos = this.totais['current-issue'];
          break;
        case 2: // Most Read
          this.total_artigos = 10; // Padrao `view all`
          this.collapse.nativeElement.style.display = 'none';
          break;
        case 3: // Recent Posts
          this.total_artigos = this.totais['recent-posts']; // Padrao `view all`
          this.collapse.nativeElement.style.display = 'none';
          break;
        case 4: // All Issue
          this.total_artigos = 0;
          // Zera o accordion no all issue
          this.expansion = 0;
          break;
        case 5: // Fast Track
          this.total_artigos = this.totais['fast-track'];
          break;
        default:
          this.collapse.nativeElement.style.display = 'flex';
          this.total_artigos = 0;
          break;
      }
      // Centralizar o scroll da navBar
      this.scrollNav(`nav_` + currentIndex);
    })

    /**
     * Tentou mudar o slide ele escuta.
     */
    this.slides.ionSlideWillChange.subscribe(evt => {
      this.expandedAll = false;


      let currentIndex = this.slides.getActiveIndex();

      // controle do texto do fast track 
      if (currentIndex == 5) {
        // display block no texto do fast-track
        let texto:any = document.getElementById('texto-fasttrack');
        texto.style.display = 'block';
        //Aplicando efeito de descer a página do texto do fast track
        if(texto.style.display == 'block'){
          setTimeout(() => {
            texto.style.top = '0px';
          }, 100);
          
        }
        
        
        
  
      }else {
        let texto:any = document.getElementById('texto-fasttrack');
        texto.style.display = 'none';
        
        texto.style.top = '-230px';
      }
      console.log(currentIndex);
    })

    // console.log(document.getElementById('tab-t0-0'));
    // document.getElementById('tab-t0-0').click = function(){
    //   console.log('teste');
    // }
  }


  //Colocar ao sair da view ionExit  para por o lockswipes false


  ionViewWillEnter() {
    // toda vez que retornar para tab Journals, liberar slide para todas as
    this.slides.lockSwipes(false);
    this.goToSlide(1);

    // this.detect.detectChanges()

   
  }

  doCarregarValidadores(): void { };

  // Escuta evento resposta de criacao de um component ion-[slide-name]
  setTotalArticles(obj_total_slide: { name: string, total: number }) {

    console.log(obj_total_slide);

    // console.log('eventos total',obj_total_slide);
    this.totais[obj_total_slide.name] = obj_total_slide.total;

    if (obj_total_slide.name && obj_total_slide.name == 'current-issue') {
      this.total_artigos = this.totais['current-issue']
      this.esconderLoading();
    }

    if (obj_total_slide.name && obj_total_slide.name == 'recent-posts') {
      this.total_artigos = this.totais['recent-posts']
    }

  }

  /**
   * Seta posicao atual do collapse
   * @param pos 
   */
  public setStep(pos) {
    this.step = pos;  
  }

  /**
   * Alterna collapses entre abertos e fechados
   */
  public toogleAll() {
    this.expandedAll = !this.expandedAll;
  }

  goToSlide(x) {
    this.slideAtivoChave = x;
    this.slides.slideTo(x, 500);

    // Centralizar o scroll da navBar
    this.scrollNav(`nav_` + x);
  }

  scrollNav(id: string) {

    let itemNavbarDom = document.getElementById(id);

    if (itemNavbarDom) {

      let divOverflow = "navbar-details";
      let xOffset = document.getElementById(id).offsetLeft;
      let widhtOverflow = document.getElementById(divOverflow).offsetWidth;


      let itemMenuWidth = null;
      let valor_calculado = null;
      let itemNavbarAtivo: any = document.getElementsByClassName("slick-active");

      if (itemNavbarAtivo[0]) {

        itemMenuWidth = itemNavbarAtivo[0].offsetWidth;

        valor_calculado = xOffset - (widhtOverflow / 2) + (itemMenuWidth / 2);


        if(this.navbarDetails.nativeElement.scroll){
          this.navbarDetails.nativeElement.scroll({
            top: 0, // Captura a distancia do topo onde deseja ser rolado
            left: valor_calculado,// Faz o mesmo do top mas em um ambito horizontal
            behavior: 'smooth' // Aqui eh onde vem toda a magica, ele suporta duas opcoes, o `smooth` e o `normal`
          })
        }

        


      }

    }
  }

  controllerSwipe() {
    this.meuslide.lockSwipes(true);
  }

  public favoritarToogle(evt: MouseEvent, artigo_id: number) {

    // console.log(this.favoriteStorage.isFavorite(artigo_id));
    // console.log(evt,artigo_id);
    this.favoriteStorage.salvar(artigo_id);

    evt.stopPropagation();
  }

  public viewAllMostRead() {
    this.total_artigos = 10;
    document.getElementById('bt_viewAllMostRead').style.display = 'none';

    let arrMemoriasContentDOM = document.getElementsByClassName(`memorias-content`);
    for (var x = 0; x < arrMemoriasContentDOM.length; x++) {
      arrMemoriasContentDOM[x].removeAttribute(`hidden`);
    }


  }

  public showAllPosts: boolean = false;
  public viewAllRecentPosts(total:number) {

    this.showAllPosts = true;
    this.events.publish('revista-details:viewAllPostsClicked',true);
    document.getElementById('bt_viewAllRecentPosts').style.display = 'none';
  }

  

}
