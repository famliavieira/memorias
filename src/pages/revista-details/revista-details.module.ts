import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevistaDetailsPage } from './revista-details';
import { MatExpansionModule } from '@angular/material';

@NgModule({
  declarations: [
    RevistaDetailsPage,
  ],
  imports: [
    MatExpansionModule,
    IonicPageModule.forChild(RevistaDetailsPage)

  ],
})
export class RevistaDetailsPageModule {}
