import { Component } from '@angular/core';
import { IonicPage, NavParams, LoadingController, AlertController, ModalController, NavController, Events } from 'ionic-angular';
import { CurrentIssue } from '../../model/currentissues.class';
import { Categoria } from '../../model/categoria.class';
import { SharedProvider } from '../../providers/shared/shared';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { FavoriteStorageProvider } from '../../providers/favorite-storage/favorite-storage';

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage extends PaginaBase {

  public categoria_id: number = 0;

  /* Artigos Current Issues e sua categoria*/
  public dadosCurrentIssue: CurrentIssue;
  public categoria: Categoria;

  /* Atributos para manipular Expansion Panel */
  public step: number = 0;
  public expandedAll: boolean = false;

  public total_favoritos: number = 0;
  // Possui dependencias(modal)
  public sharedProvider: SharedProvider;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public webservice: WebserviceProvider,
    public favoriteStorage: FavoriteStorageProvider,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public events: Events,
    
  ) {
    super({
      modalCtrl: modalCtrl,
      alertCtrl: alertCtrl,
      loadingCtrl: loadingCtrl
    });

    // Possui dependencias(modal)
    this.sharedProvider = new SharedProvider(modalCtrl);

    this.categoria = new Categoria();
    this.dadosCurrentIssue = new CurrentIssue();

    if (this.navParams.get('categoria_id')) {
      this.categoria_id = this.navParams.get('categoria_id');
    }



    this.events.subscribe('favoriteStorage:deleted', (obj: any) => {
      this.total_favoritos = this.contarFavoritosPorArtigos();
    })

    this.events.subscribe('favoriteStorage:inserted', (obj: any) => {
      this.total_favoritos = this.contarFavoritosPorArtigos();
    })


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');


  }




  ngOnInit() {

    console.log(this.categoria_id);

    // Consumir endpoint "Current issue do webservice
    this.webservice
      .getCategory(this.categoria_id)
      .then((resJsonCurrentIssue: CurrentIssue) => {

        // console.log('jsonCurrentIssue', resJsonCurrentIssue);

        // Titulo do Current Issue
        this.dadosCurrentIssue.titulo = resJsonCurrentIssue.titulo;
        this.dadosCurrentIssue.total = resJsonCurrentIssue.total;

        // Categoria dos artigos
        this.categoria.descricao = resJsonCurrentIssue.categoria_descricao;
        this.categoria.imagem = resJsonCurrentIssue.categoria_imagem;
        this.categoria.nome = resJsonCurrentIssue.categoria_nome;

        // preencher com categoria
        this.dadosCurrentIssue.categoria = this.categoria;

        // dados remontados para um array de objetos
        this.dadosCurrentIssue.dados = this.montarArrayArtigosPorTipo(resJsonCurrentIssue.dados);

        this.dadosCurrentIssue.ids = resJsonCurrentIssue.ids;

        this.total_favoritos = this.contarFavoritosPorArtigos()
        // this.EvtPegarTotal.emit({ 'name': 'category', 'total': resJsonCurrentIssue.total });

        // Popular total de artigos se for o slide 'ahead'
        // if (this.slideAtivoChave == 1) {
        //   this.total_artigos = this.dadosCurrentIssue.total;
        // }

        // this.splashScreen.hide();

      });

  }

  /**
   * Seta posicao atual do collapse
   * @param pos 
   */
  public setStep(pos) {
    this.step = pos;
  }


  montarArrayArtigosPorTipo(artigos) {

    let array_tipos_artigos = new Array();
    let tiposArtigos = this.listarTiposDeArtigos(artigos);
    let index = 0;

    for (var tipo_id in tiposArtigos) {

      array_tipos_artigos[index] = {
        tipo_nome: tiposArtigos[tipo_id][0].tipo_artigo_nome,
        tipo: tipo_id,
        artigos: tiposArtigos[tipo_id]
      }

      index++;
    }

    return array_tipos_artigos;

  }

  listarTiposDeArtigos(artigos) {
    // pegar os tipos
    let colecaoTiposArquivos = {}

    for (var i = 0; i < artigos.length; i++) {

      if (!colecaoTiposArquivos[artigos[i].tipo_artigo_id]) {
        colecaoTiposArquivos[artigos[i].tipo_artigo_id] = new Array();
      }
      // colecaoTiposArquivos["nome_do_tipo"] = artigos[i]
      colecaoTiposArquivos[artigos[i].tipo_artigo_id].push(artigos[i]);
    }

    return colecaoTiposArquivos;
  }



  public contarFavoritosPorArtigos(): number {

    let count = 0;
    let favoritos = this.favoriteStorage.listaIdsFavoritos;

    for (let index = 0; index < favoritos.length; index++) {

      const element = favoritos[index];

      if (this.dadosCurrentIssue.ids.indexOf(element) != -1) {
        count++;
      }

    }
    console.log(count);
    return count;

  }


  doCarregarValidadores(): void { }
}
