import { Component, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';

/**
 * Generated class for the RevistasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revistas',
  templateUrl: 'revistas.html'
})
export class RevistasPage {

  heartActive: boolean = false;


  @Output() _clickEvent: EventEmitter<any> = new EventEmitter();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private webservice: WebserviceProvider
  ) {

    this.webservice
      .setMenu('current')
      .listarRevistas()
      .then(response => {
        console.log(response);
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevistasPage');
  }

  toogleClassHeart(evt, elemento) {
    this.heartActive = !this.heartActive;
  }

  emitClick() {
    alert(1);
    this._clickEvent.emit()
  }


  myIcon: string;
  mudarNome(evt) {
    this.myIcon = "heart";
    let classes = evt.target.getAttribute('class') + ' active';
    evt.target.setAttribute(classes);
    console.log(evt);
  }
}
