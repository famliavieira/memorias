import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MemoriasBoardPage } from './memorias-board';

@NgModule({
  declarations: [
    MemoriasBoardPage,
  ],
  imports: [
    IonicPageModule.forChild(MemoriasBoardPage),
  ],
})
export class MemoriasBoardPageModule {}
