import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecentPostsPage } from './recent-posts';

@NgModule({
  declarations: [
    RecentPostsPage,
  ],
  imports: [
    IonicPageModule.forChild(RecentPostsPage),
  ],
})
export class RecentPostsPageModule {}
