import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MostRead } from '../../model/mostread.class';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the FavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recent-posts',
  templateUrl: 'recent-posts.html',
})
export class RecentPostsPage {


  public dadosMostRead: MostRead;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private webservice: WebserviceProvider,
    public sharedProvider: SharedProvider
    ) {

      this.dadosMostRead = new MostRead();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritePage');
  }
  ngOnInit(){
    // Consumir endpoint "MostRead" do webservice
    this.webservice
      .listarMostRead()
      .then((resJsonAhead: MostRead) => {

        console.log(resJsonAhead);
        this.dadosMostRead = resJsonAhead;


      });
  }



  montarArrayArtigosPorTipo(artigos) {

    let array_tipos_artigos = new Array();
    let tiposArtigos = this.listarTiposDeArtigos(artigos);
    let index = 0;

    for (var tipo_id in tiposArtigos) {

      array_tipos_artigos[index] = {
        tipo_nome: tiposArtigos[tipo_id][0].tipo_artigo_nome,
        tipo: tipo_id,
        artigos: tiposArtigos[tipo_id]
      }

      index++;
    }

    return array_tipos_artigos;

  }

  listarTiposDeArtigos(artigos) {
    // pegar os tipos
    let colecaoTiposArquivos = {}

    for (var i = 0; i < artigos.length; i++) {

      if (!colecaoTiposArquivos[artigos[i].tipo_artigo_id]) {
        colecaoTiposArquivos[artigos[i].tipo_artigo_id] = new Array();
      }
      // colecaoTiposArquivos["nome_do_tipo"] = artigos[i]
      colecaoTiposArquivos[artigos[i].tipo_artigo_id].push(artigos[i]);
    }

    return colecaoTiposArquivos;
  }

}
