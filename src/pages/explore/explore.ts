import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { MemoriasBoardPage } from '../memorias-board/memorias-board';
import { EditorialPolicyPage } from '../editorial-policy/editorial-policy';
import { RecentPostsPage } from '../recent-posts/recent-posts';

/**
 * Generated class for the ExplorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html',
})
export class ExplorePage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController
    ) {
  }

  public goToPage(destino){

    switch(destino){
      case 'recents':
        destino = RecentPostsPage;
      break;
      case 'memory':
        destino = MemoriasBoardPage;
      break;
      case 'policy':
        destino = EditorialPolicyPage;
      break;
    }
    let modal = this.modalCtrl.create(destino);
    modal.present();
    // console.log('asdasd aeeeeeeeee');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExplorePage');
  }

}
