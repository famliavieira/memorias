import { Component, ViewChild } from '@angular/core';
import { SearchPage } from '../search/search';
import { ExplorePage } from '../explore/explore';
import { FavoritePage } from '../favorite/favorite';
import { RevistaDetailsPage } from '../revista-details/revista-details';
import { EditorialPolicyPage } from '../editorial-policy/editorial-policy';
import { RecentPostsPage } from '../recent-posts/recent-posts';
import { Tabs } from 'ionic-angular';

@Component({
  selector: 'tabs-page',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('myTabs') tabRef: Tabs;

  tab1Root = RevistaDetailsPage;
  tab2Root = SearchPage;
  tab3Root = ExplorePage;  
  tab4Root = FavoritePage;
  tab5Root = RecentPostsPage;

  constructor() {

  }

  public toFirstTab(){
    this.tabRef.select(0);
  }


}
