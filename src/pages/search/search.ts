import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, IonicTapInput } from 'ionic-angular';
import { WebserviceProvider } from '../../providers/webservice/webservice';
import { PaginaBase } from '../../infraestrutura/PaginaBase';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage extends PaginaBase {

  listSearch: Array<any> = [];
  qntSearch: number = 0;

  inputSearch: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public webservice: WebserviceProvider,
    public loadingCtrl: LoadingController
    ) {
      super({loadingCtrl:loadingCtrl})
      // this.webservice.search('dengue').then( (results)=>{
      //   console.log(results)
      //   this.listSearch = results.dados;
      // });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  submitSearch(x?){
    this.mostrarLoading('Searching Results').then( ()=>{

      if(x == 'clean'){
        this.webservice.search('').then( (results)=>{
          console.log(results)
          this.listSearch = results.dados;
          this.listSearch = [];
          
          this.esconderLoading();
        });
      }else{
        this.webservice.search(this.inputSearch).then( (results)=>{
          console.log(results)
          this.listSearch = results.dados;
          if(this.listSearch == null){
            this.qntSearch = 0;
          }else{
            this.qntSearch = this.listSearch.length;
          }
          

          this.esconderLoading();
        });
      }

    })
    
  }



  //Chamado quando clicar no X para limpar o input
  submitClean(){
    let input:any = document.getElementById('input-search').firstElementChild;
    input.value = '';
  }

  doCarregarValidadores():void{}

}