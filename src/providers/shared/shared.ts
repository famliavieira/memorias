import { Injectable } from '@angular/core';
import { PaginaBase } from '../../infraestrutura/PaginaBase';
import { ArticlePage } from '../../pages/article/article';
import { ModalController } from 'ionic-angular';
import { CategoryPage } from '../../pages/category/category';
import { PostPage } from '../../pages/post/post';

/*
  Generated class for the SharedProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SharedProvider extends PaginaBase {

  constructor(
    public modalCtrl: ModalController) {
    super({modalCtrl: modalCtrl})
    console.log('Hello SharedProvider Provider');
  }

  goToArticlePage(artigo_id:number){
    console.log('artigo_id')
    return this.mostrarModal(ArticlePage,{artigo_id:artigo_id});
  }

  goToCategoryPage(categoria_id:number){
    console.log('categoria_id',categoria_id);
    return this.mostrarModal(CategoryPage,{categoria_id:categoria_id});
  }

  goToPostPage(post_id:number){
    console.log('post_id')
    return this.mostrarModal(PostPage,{post_id:post_id});
  }

  doCarregarValidadores():void{}

}
