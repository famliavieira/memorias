import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FastTrack } from '../../model/fasttrack.class';
import { AllIssues, PatternToArrayIssuesAno } from '../../model/allissues.class';
import { Artigos } from '../../model/artigos.class';
import { Jornauls } from '../../model/journals.class';
import { MostRead } from '../../model/mostread.class';
import { CurrentIssue } from '../../model/currentissues.class';
import { Categoria } from '../../model/categoria.class';
import { AheadOfPrint } from '../../model/aheadofprint.class';
import { RecentPost } from '../../model/recentpost.class';

/*
  Generated class for the WebserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WebserviceProvider {

  // private url_base: string = "http://memorias.wda.ag/components/com_memorias/mem_app_ws.php";
  private url_base: string = "http://memorias.ioc.fiocruz.br/components/com_memorias/mem_app_ws.php";
  private token = "qR3fgtER419fgkioAo1p";
  private menu: string = "current";
  private url_endpoint: string = "";


  public dadosAheadOfPrint: AheadOfPrint;


  /* Artigos Current Issues e sua categoria*/
  public dadosCurrentIssue: CurrentIssue;
  public categoria: Categoria;

  public dadosMostRead: MostRead;
  public dadosRecentPost: RecentPost;

  constructor(public http: HttpClient) {
    // console.log('Hello WebserviceProvider Provider');
    this.categoria = new Categoria();
    this.dadosCurrentIssue = new CurrentIssue();
    this.dadosMostRead = new MostRead();
    this.dadosAheadOfPrint = new AheadOfPrint();
    this.dadosRecentPost = new RecentPost();
  }

  listarAheadOfPrint(): Promise<AheadOfPrint> {

    return new Promise((resolve, reject) => {
      
      this.setMenu('ahead').montaEndPoint();
      // console.log(this.url_endpoint);
      // this.url_endpoint = 'assets/json/dados.json';     // teste
      this.http.get(this.url_endpoint).subscribe((resJsonAhead: AheadOfPrint) => {

        this.dadosAheadOfPrint.dados = this.montarArrayArtigosPorTipo(resJsonAhead.dados);
        this.dadosAheadOfPrint.total = resJsonAhead.total;

        resolve(this.dadosAheadOfPrint);

      });

    })

  }

  listarRevistas(): Promise<any> {

    return new Promise((resolve, reject) => {

      this.setMenu('current').montaEndPoint();
      // this.url_endpoint = 'assets/json/dados.json'; // teste
      this.http.get(this.url_endpoint).subscribe((resJsonCurrentIssue: CurrentIssue) => {

        console.log(resJsonCurrentIssue)
        // Titulo do Current Issue
        this.dadosCurrentIssue.titulo = resJsonCurrentIssue.titulo;
        this.dadosCurrentIssue.total = resJsonCurrentIssue.total;
        this.dadosCurrentIssue.ids = resJsonCurrentIssue.ids;

        // Categoria dos artigos
        this.categoria.descricao = resJsonCurrentIssue.categoria_descricao;
        this.categoria.imagem = resJsonCurrentIssue.categoria_imagem;
        this.categoria.nome = resJsonCurrentIssue.categoria_nome;

        // preencher com categoria
        this.dadosCurrentIssue.categoria = this.categoria;

        // dados remontados para um array de objetos
        this.dadosCurrentIssue.dados = montarArray(resJsonCurrentIssue.dados);

        console.log(this.dadosCurrentIssue);
        resolve(this.dadosCurrentIssue);

      });
    });


    function montarArray(artigos) {

      let array_tipos_artigos = new Array();
      let tiposArtigos = listarTipos(artigos);
      let index = 0;

      console.log('tiposArtigos',tiposArtigos)
  
      for (var tipo_id in tiposArtigos) {
  
        array_tipos_artigos[index] = {
          tipo_nome: tiposArtigos[tipo_id][0].tipo_artigo_nome,
          tipo: tipo_id,
          artigos: tiposArtigos[tipo_id]
        }
  
        index++;
      }
  
      return array_tipos_artigos;
  
    }


    function listarTipos(artigos) {
      // pegar os tipos
      let colecaoTiposArquivos = {}
  
      for (var i = 0; i < artigos.length; i++) {
  
        if (!colecaoTiposArquivos[artigos[i].tipo_artigo_nome]) {
          colecaoTiposArquivos[artigos[i].tipo_artigo_nome] = new Array();
        }
        // colecaoTiposArquivos["nome_do_tipo"] = artigos[i]
        colecaoTiposArquivos[artigos[i].tipo_artigo_nome].push(artigos[i]);
      }
  
      return colecaoTiposArquivos;
    }

  }

  listarMostRead(): Promise<MostRead> {

    return new Promise((resolve, reject) => {

      this.setMenu('mostread').montaEndPoint();
      // console.log(this.url_endpoint);
      // this.url_endpoint = 'assets/json/dados.json';     // teste
      return this.http.get(this.url_endpoint).subscribe((resJsonAhead: MostRead) => {
        console.log(resJsonAhead);

        let arrayTiposArtigos = this.listarTiposDeArtigos(resJsonAhead.dados);

        console.log(arrayTiposArtigos);

        this.dadosMostRead.dados = resJsonAhead.dados
        this.dadosMostRead.total = resJsonAhead.total;

        resolve(this.dadosMostRead);
      })

    });


  }

  listarFastTrack(): Promise<FastTrack> {

    return new Promise((resolve, reject) => {

      this.setMenu('fasttrack').montaEndPoint();
      return this.http.get(this.url_endpoint)
        .subscribe((resJsonFastTrack: any) => {

          let totalNaoPublicado = resJsonFastTrack.nao_publicado.total;
          let totalPublicado = resJsonFastTrack.publicado.total;

          delete resJsonFastTrack.publicado['total'];
          delete resJsonFastTrack.nao_publicado['total'];

          let objFastTrack = new FastTrack();


          objFastTrack.publicado = new Array();
          objFastTrack.nao_publicado = new Array();

          for (let p in resJsonFastTrack.publicado) {
            objFastTrack.publicado.push(resJsonFastTrack.publicado[p]);
            // console.log(resJsonFastTrack.publicado[p])
          }

          for (let x in resJsonFastTrack.nao_publicado) {
            // console.log(resJsonFastTrack.nao_publicado[x])
            objFastTrack.nao_publicado.push(resJsonFastTrack.nao_publicado[x]);
          }


          // objFastTrack.publicado = resJsonFastTrack.publicado;
          // objFastTrack.nao_publicado = resJsonFastTrack.nao_publicado;
          objFastTrack.total = (totalNaoPublicado + totalPublicado) as number;

          // console.log(objFastTrack);
          resolve(objFastTrack);
        });
    })
  }

  listarRecentPost(): Promise<RecentPost> {

    return new Promise((resolve, reject) => {

      this.setMenu('recentpost').montaEndPoint();
      return this.http.get(this.url_endpoint).subscribe((resJsonRecentPost: RecentPost) => {
        console.log(resJsonRecentPost);

        this.dadosRecentPost.dados = resJsonRecentPost.dados
        this.dadosRecentPost.total = resJsonRecentPost.total;

        resolve(this.dadosRecentPost);
      })

    });


  }


  listarAllIssues(paginacao: number = 1) {

    return new Promise((resolve, reject) => {

      this.setMenu('allissues').montaEndPoint();

      let url_allissues_por_pagina = this.url_endpoint + '&pagina=' + paginacao;

      return this.http.get(url_allissues_por_pagina)
        .subscribe((resJsonAllIssues: any) => {
          // Objeto que trata de allIssues
          let objAllIssues = new AllIssues();

          // Populando retorno do endpoint, no ObjAllIssues.
          objAllIssues.setIssues(resJsonAllIssues);

          // Resolvendo Promise com array de Issues por Ano, em ordem inversa
          resolve(objAllIssues);
        });
    })

  }

  getArticle(artigo_pai: number) {

    // console.log(artigo_pai);

    this
      .setMenu('article')
      .montaEndPoint();

    let urlArticleID = this.url_endpoint + '&id=' + artigo_pai;

    return new Promise((resolve, reject) => {

      this.http.get(urlArticleID)
        .subscribe((resJsonArticle) => {
          resolve(resJsonArticle);
        });

    })
  }

  getPost(post_id: number) {

    // console.log(artigo_pai);

    this
      .setMenu('post')
      .montaEndPoint();

    let urlPostID = this.url_endpoint + '&id=' + post_id;

    return new Promise((resolve, reject) => {

      this.http.get(urlPostID)
        .subscribe((resJsonPost) => {
          resolve(resJsonPost);
        });

    })
  }

  getCategory(category_id: number) {

    // console.log(artigo_pai);
    this
      .setMenu('category')
      .montaEndPoint();

    let urlCategoryID = this.url_endpoint + '&id=' + category_id;

    return new Promise((resolve, reject) => {

      this.http.get(urlCategoryID)
        .subscribe((resJsonCategory) => {
          resolve(resJsonCategory);
        });

    })

  }

  search(findParam: string): Promise<Jornauls> {
    // console.log(artigo_pai);
    this
      .setMenu('search')
      .montaEndPoint();

    let urlSearchParams = this.url_endpoint + '&find=' + findParam;

    return new Promise((resolve, reject) => {

      this.http.get(urlSearchParams)
        .subscribe((resJsonSearch: Jornauls) => {

          // let resultadoEmArray = this.montarArrayArtigosPorTipo(resJsonSearch.dados);
          // console.log(resultadoEmArray);
          resolve(resJsonSearch);
        });
    });


  }



  private montaEndPoint() {

    if (!this.menu) {
      throw ("A URL necessita de um valor para o menu");
    }

    const http = this.url_base;
    const token_param = "?token=" + this.token;
    const menu_param = "&menu=" + this.menu;

    this.url_endpoint = http + token_param + menu_param;

    return this.url_endpoint;
  }


  getMenu() {
    return this.menu;
  }

  setMenu(menu: string) {
    this.menu = menu;
    return this;
  }



  private montarArrayArtigosPorTipo(artigos) {


    if(artigos!==undefined){
      let array_tipos_artigos = new Array();
      let tiposArtigos = this.listarTiposDeArtigos(artigos);
      let index = 0;
  
      for (var tipo_id in tiposArtigos) {
  
        array_tipos_artigos[index] = {
          tipo_nome: tiposArtigos[tipo_id][0].tipo_artigo_nome,
          tipo: tipo_id,
          artigos: tiposArtigos[tipo_id]
        }
  
        index++;
      }
  
      return array_tipos_artigos;
  
    }

    
  }

  private listarTiposDeArtigos(artigos) {
    // pegar os tipos
    let colecaoTiposArquivos = {}

    for (var i = 0; i < artigos.length; i++) {

      if (!colecaoTiposArquivos[artigos[i].tipo_artigo_id]) {
        colecaoTiposArquivos[artigos[i].tipo_artigo_id] = new Array();
      }
      // colecaoTiposArquivos["nome_do_tipo"] = artigos[i]
      colecaoTiposArquivos[artigos[i].tipo_artigo_id].push(artigos[i]);
    }

    return colecaoTiposArquivos;
  }



}
