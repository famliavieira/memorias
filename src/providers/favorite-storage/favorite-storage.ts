import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Article } from '../../model/article.class';
import { Storage } from '@ionic/storage';
import { WebserviceProvider } from '../webservice/webservice';
import { Events } from 'ionic-angular';

/*
  Generated class for the FavoriteStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FavoriteStorageProvider {

  listaIdsFavoritos: number[] = [];

  constructor(
    public http: HttpClient,
    public storage: Storage,
    public webservice: WebserviceProvider,
    public events: Events

  ) {
    // console.log('Hello FavoriteStorageProvider Provider');

    this.getAll().then((arrayIdsFavoritos) => {
      console.log(`meus favoritos`, this.listaIdsFavoritos);
    })
  }

  salvar(article_id: number): Promise<any> {

    // console.log(`id`, article_id);
    let msg = ``;
    if (this.isFavorite(article_id)) {
      if (this.removerFavorito(article_id)) {
        msg = `REMOVEU DE FAVORITOS`;
        
      }

    } else {
      this.listaIdsFavoritos.push(article_id);
      this.events.publish('favoriteStorage:inserted', { artigo_id: article_id, deletadoEm: Date.now() })
      
      msg = `INCLUIDO NO FAVORITOS`;
    }

    return this.storage.set('favorites', JSON.stringify(this.listaIdsFavoritos)).then((res) => {
      // console.log(res,msg);
    });



  }

  getAll(): Promise<number[]> {
    return this.storage.get('favorites').then((resultJson) => {
      if (!resultJson) {
        return [];
      }

      let result = JSON.parse(resultJson);

      this.listaIdsFavoritos = result;

      return result;
    })
  }

  removerFavorito(artigo_id: number): boolean {

    

    let index = this.listaIdsFavoritos.findIndex((idFavorito) => {
      return idFavorito === artigo_id;
    });

    if (index !== -1) {
      this.listaIdsFavoritos.splice(index, 1);
      this.events.publish('favoriteStorage:deleted', { artigo_id: artigo_id, deletadoEm: Date.now() })
      return true;
    }

    return false;
  }


  public isFavorite(artigo_id: number): boolean {

    // console.log(this.listaIdsFavoritos.findIndex((idFavorito) => {
    //   return idFavorito === artigo_id;
    // }) !== -1 ? true : false)

    return this.listaIdsFavoritos.findIndex((idFavorito) => {
      return idFavorito === artigo_id;
    }) !== -1 ? true : false;

  }

  public listarFavoritos() {

    let arr_promises = [];

    for (var i = 0; i < this.listaIdsFavoritos.length; i++) {
      let p = this.webservice.getArticle(this.listaIdsFavoritos[i]);
      arr_promises.push(p);
    }
    return Promise.all(arr_promises);


  }

}
